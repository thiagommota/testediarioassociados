/*Função ativada ao digitar na caixa de pesquisa*/
function pesquisa() {

    var caixa = document.getElementById("caixaPesquisa");
    var span = document.getElementById("itemPesquisa");
    var nResultado = document.getElementById("nResultados");

    /*Preenche a lateral com o que está escrito na caixa de pesquisa*/
    span.textContent = caixa.value;

    /*Se o valor da caixa de pesquisa for igual a "Lula"*/
    if (caixa.value == "Lula") {
        /*Apresenta o número 4*/
        nResultado.textContent = "4 ";
    }
    /*Caso contrário*/
    else {
        /*Apresenta o valor 0*/
        nResultado.textContent = "0 ";
    }

    /*Se o valor da caixa de pesquisa for igual a "Lula"*/
    if (caixa.value == "Lula") {
        /*Mostra as matérias relacionadas*/
        document.getElementById("cards").style.display = "block";
    }
    /*Caso contrário*/
    else {
        /*Não mostra nada*/
        document.getElementById("cards").style.display = "none";
    }

}

/*Função ativada ao clicar no botão de mais matérias*/
function maisResultados() {

    /*Apresenta a seguinte mensagem no span correspondente*/
    var mensagem = document.getElementById("semResultados").textContent = "Não existem mais resultados para essa pesquisa";
}